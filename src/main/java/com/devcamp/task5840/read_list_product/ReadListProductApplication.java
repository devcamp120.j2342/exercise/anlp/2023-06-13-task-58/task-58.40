package com.devcamp.task5840.read_list_product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReadListProductApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReadListProductApplication.class, args);
	}

}
