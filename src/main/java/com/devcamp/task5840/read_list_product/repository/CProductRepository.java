package com.devcamp.task5840.read_list_product.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task5840.read_list_product.model.CProduct;

public interface CProductRepository extends JpaRepository<CProduct, Long>{
    
}
