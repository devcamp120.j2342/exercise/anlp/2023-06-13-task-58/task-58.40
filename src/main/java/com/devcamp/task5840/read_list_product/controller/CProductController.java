package com.devcamp.task5840.read_list_product.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.*;

import com.devcamp.task5840.read_list_product.model.CProduct;
import com.devcamp.task5840.read_list_product.repository.CProductRepository;

@CrossOrigin
@RestController
public class CProductController {
    @Autowired
    CProductRepository pProductRepository;

    @GetMapping("/products")
    public ResponseEntity<List<CProduct>> getAllProduct() {
        try{
            List<CProduct> pProduct = new ArrayList<CProduct>();
            pProductRepository.findAll().forEach(pProduct::add);
            return new ResponseEntity<>(pProduct, HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
